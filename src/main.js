import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import Home from './pages/Home.vue'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { faInstagram, faLinkedin, faGithub, faBitbucket, faReact, faJsSquare, faHtml5, faAndroid, faGitSquare, faVuejs, faCss3Alt, faNodeJs} from '@fortawesome/free-brands-svg-icons'
import { faArrowRight, faBook, faTasks, faUser, faEnvelope } from '@fortawesome/free-solid-svg-icons'

library.add(
  faInstagram, 
  faLinkedin, 
  faGithub, 
  faBitbucket, 
  faUser, 
  faTasks, 
  faBook,
  faEnvelope, 
  faReact,
  faJsSquare,
  faHtml5,
  faNodeJs,
  faAndroid,
  faGitSquare,
  faVuejs,
  faCss3Alt,
  faArrowRight)
Vue.component('font-awesome-icon', FontAwesomeIcon)

// Vue.config.productionTip = false

Vue.use(VueRouter)

const routes = [
  { path: '/', component: Home}
];

const router = new VueRouter({
  routes
})

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
